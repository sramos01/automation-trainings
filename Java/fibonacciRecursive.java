import java.util.ArrayList;

public class fibonacciRecursive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 1.	Write a program using recursive methods to generate Fibonacci series
				//Eg. Fib(count=5):1,2,3,5,8
		int cont=5;
		for(int i=1;i<=cont;i++) {
			System.out.print(Fib(i)+" ");
		}		
	}
	public static int Fib(int count) {
		if(count == 0) return 0;
		if(count == 1) return 1;
		return Fib(count-1)+Fib(count-2);
		
	}

}
