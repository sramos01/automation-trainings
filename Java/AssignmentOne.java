import java.util.ArrayList;

public class AssignmentOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1. Write a Java Main program to create the concatenation of the two strings str1: hot and str2:dog
		String str1="hot"; 
		String str2="dog";
		String conStr= str1.concat(str2);
		System.out.println("Part 1 OUTPUT:");
		System.out.println(conStr);		
		//2. Write a user defined method which take 3 integer parameter and return highest number
			//a. Call this method in main method and print output
				//getGreatestnNum(23,12,19) and getGreatestNum(122,98,377)
		System.out.println("Part 2 OUTPUT:");
		System.out.println(getGreatestNum(23,12,19));
		System.out.println(getGreatestNum(122,98,377));
		//3. Write a user defined method using Java loop to print fibonacci series till N number
			//a. Call this method in Main method and print output
		System.out.println("Part 3 OUTPUT:");
		System.out.println(printFibonacciNum(8).toString().replace("[","").replace("]", ""));
		System.out.println(printFibonacciNum(25).toString().replace("[","").replace("]", ""));
				//get printfibonacciNum(8) and printFibonacciNum(25)
		//4. Write overloaded method, one find greatest number among three numbers and another find greatest number among 4 numbers
			//a. Call this method in main method and print output
		System.out.println("Part 4 OUTPUT:");
		System.out.println(getGreatestNum(23,12,19,50));
	}
	public static int getGreatestNum(int a, int b, int c) {
		int greatest=a;
		if (b>greatest) greatest=b;
		if (c>greatest) greatest=c;
		return greatest;
	}
	
	public static ArrayList<Integer> printFibonacciNum(int n) {
		ArrayList<Integer> fibonacci = new ArrayList<Integer>();
		int j=1; fibonacci.add(j);
		int k=1; fibonacci.add(k);
		for(int i=1;i<n;i++) {
			int addition=j+k;
			fibonacci.add(addition);
			j=k; k=addition;
		}
			return fibonacci;
		}
	
	public static int getGreatestNum(int a, int b, int c, int d) {
		int greatest=a;
		if (b>greatest) greatest=b;
		if (c>greatest) greatest=c;
		if (d>greatest) greatest=d;
		return greatest;
	
	}
}
