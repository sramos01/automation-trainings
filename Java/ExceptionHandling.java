
public class ExceptionHandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//2.	Write java program to show exception handling if:
			//a.	Divide number by zero
		int a = 1; int b = 0;
		try {
			System.out.println(a/b);
		}
		catch(ArithmeticException e){
			System.out.println("You cannot divide by zero");
		}
		// b.	Assigning a string to null and try to concate to another string
		String str1=null; String str2="dog";
		if(str1==null||str2==null) {
			System.out.println("Cannot concate, one string is null");
		}
		else {
			System.out.println(str1.concat(str2));
		}
		
		
		
	}

}
