package partThree;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Studentdataclass {

	public double studentId;
	public String firstName, middleName, lastName;
	LocalDate dob;
	public Studentdataclass(double studentId, String firstName, String middleName, String lastName, String dobs) {
		super();
		this.studentId = studentId;
		this.firstName = firstName+" ";
		this.middleName = middleName+" ";
		this.lastName = lastName+" ";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		this.dob= LocalDate.parse(dobs, formatter);		
	}
	public String conNames() {		
		return (this.firstName.concat(this.middleName).concat(this.lastName));
	}
	
	public String displayDate() {
		String birth = dob.format(DateTimeFormatter.ofPattern("yyyy-dd-MM"));
		return birth;		
	}
	
	
}
