package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class formpageClick {

	public static Boolean highestEducation(WebDriver driver) {
		formpage.getToPage(driver);
		driver.findElement(By.id("radio-button-2")).click();
		Boolean enabled =  driver.findElement(By.id("radio-button-2")).isSelected();
		return enabled;
	}
	
	public static Boolean sex(WebDriver driver) {
		formpage.getToPage(driver);
		driver.findElement(By.id("checkbox-2")).click();
		Boolean enabled =  driver.findElement(By.id("checkbox-2")).isSelected();
		return enabled;
	}
	
	public static String dropdown(WebDriver driver, String value) {
		formpage.getToPage(driver);
		Select dropdown = new Select(driver.findElement(By.id("select-menu")));
		dropdown.selectByValue(value);
		String value1 = driver.findElement(By.id("select-menu")).getAttribute("value");
		return value1;
	}
}
