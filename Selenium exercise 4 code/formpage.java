package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class formpage {	
	
	public static void getToPage(WebDriver driver) {
		driver.get("https://formy-project.herokuapp.com/form");
	}	
	public static String firstName(WebDriver driver, String firstName) {
		getToPage(driver);
		driver.findElement(By.id("first-name")).sendKeys(firstName);
		String name= driver.findElement(By.id("first-name")).getAttribute("value");
		return name;		
	}	
	public static String lastName(WebDriver driver, String lastName) {
		getToPage(driver);
		driver.findElement(By.id("last-name")).sendKeys(lastName);
		String lastName1 = driver.findElement(By.id("last-name")).getAttribute("value");
		return lastName1;
	}	
	public static String jobTitle(WebDriver driver, String jobTitle) {
		getToPage(driver);
		driver.findElement(By.id("job-title")).sendKeys(jobTitle);
		String jobTitle1 = driver.findElement(By.id("job-title")).getAttribute("value");
		return jobTitle1;
	}	
	public static String date(WebDriver driver, String date) {
		getToPage(driver);
		driver.findElement(By.id("datepicker")).sendKeys(date);
		String date1 = driver.findElement(By.id("datepicker")).getAttribute("value");
		return date1;
	}
}
