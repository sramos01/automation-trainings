package pageTest;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.*;

import pages.formpage;
import pages.formpageClick;
import pages.submitPage;


public class FunctionalityTest {
	
	public WebDriver init() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DANY_\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}
	
	@Test
	public void testGetPage() {		
		WebDriver driver = init();
		formpage.getToPage(driver);
		assertEquals("https://formy-project.herokuapp.com/form",driver.getCurrentUrl());
		
	}
	
	@Test 
	public void testFirstName() {
		WebDriver driver = init();
		String firstName = "Daniela";
		String name = formpage.firstName(driver, firstName);
		assertEquals(firstName,name);
		driver.quit();
		
	}
	
	@Test 
	public void testLastName() {
		WebDriver driver = init();
		String lastName = "Ramos";
		String lastName1 = formpage.lastName(driver, lastName);
		assertEquals(lastName,lastName1);
		driver.quit();
		
	}
	
	@Test 
	public void testJobTitle() {
		WebDriver driver = init();	
		String jobTitle = "Automation trainee";
		String jobTitle1 = formpage.jobTitle(driver, jobTitle);
		assertEquals(jobTitle,jobTitle1);
		driver.quit();		
	}
	
	@Test 
	public void testDate() {
		WebDriver driver = init();	
		String date = "01/11/2021";
		String date1 = formpage.date(driver, date);
		assertEquals(date,date1);
		driver.quit();		
	}
	
	@Test 
	public void testHighestEducation() {
		WebDriver driver = init();
		Boolean enabled = formpageClick.highestEducation(driver);
		assertTrue(enabled);
		driver.quit();
	}
	
	@Test 
	public void testSex() {
		WebDriver driver = init();
		Boolean enabled = formpageClick.sex(driver);
		assertTrue(enabled);
		driver.quit();
	}
	
	@Test
	public void testDropdown() {
		WebDriver driver = init();
		String value = "1";
		String value1 = formpageClick.dropdown(driver, value);
		assertEquals(value,value1);		
		driver.quit();
	}
	
	@Test
	public void testThanks() {
		WebDriver driver = init();
		String text = submitPage.thanksMessage(driver);
		assertEquals("Thanks for submitting your form", text);
		driver.quit();		
	}
}
