package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class submitPage {

	public static String thanksMessage(WebDriver driver) {
		formpage.getToPage(driver);
		driver.findElement(By.xpath("//a[@role='button']")).click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'form')]")));
		driver.findElement(By.xpath("//div[contains(text(),'form')]")).getText();		
		String text = driver.findElement(By.xpath("//h1[contains(text(),'Thank')]")).getText();
		return text;
	}

}
