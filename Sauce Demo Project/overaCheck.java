package AutomationPractice.AP;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class overaCheck {
	public static returnoveraCheck productMatches(WebDriver driver) {
		ArrayList<String> productInfo = addToCart.addToCartn(driver, 1);
		String productPrice= productInfo.get(2).replace("$",""); float productPricen =Float.parseFloat(productPrice);  
		checkoutInfo.personalInfo(driver, 6, "Maria", "Perez", "45690");
		String itemName = driver.findElement(By.xpath("//div[@class='inventory_item_name']")).getText();
		String itemPrice = driver.findElement(By.xpath("//div[@class='inventory_item_price']")).getText();
		float itemPricen = Float.parseFloat(itemPrice.replace("$","")); 
		String tax = driver.findElement(By.xpath("//div[contains(@class,'tax')]")).getText();
		float taxn = Float.parseFloat(tax.replace("Tax: $",""));
		String totalPrice = driver.findElement(By.xpath("(//div[contains(@class,'total')])[2]")).getText();
		float totaln = Float.parseFloat(totalPrice.replace("Total: $",""));
		returnoveraCheck send = new returnoveraCheck( productInfo.get(1), itemName, productPricen, itemPricen, taxn, totaln);
		return send;
	}	
	public static String finishButton(WebDriver driver) {
		driver.findElement(By.id("finish")).click();
		 return driver.findElement(By.tagName("h2")).getText();
	}
	
}
