package AutomationPractice.AP;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class overaPage {
	public static String logOut(WebDriver driver) {
		initPage.signin(driver, "standard_user", "secret_sauce");
		driver.findElement(By.xpath("//button[contains(@id,'menu')]")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
		ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@id,'logout')]")));
		driver.findElement(By.xpath("//a[contains(@id,'logout')]")).click();	
		String url = driver.getCurrentUrl();
		return url;
	}
	public static String[] resetApp(WebDriver driver) {
		initPage.signin(driver, "standard_user", "secret_sauce");
		String itemsNoS ="";
		ArrayList<String> itemsN =  addToCart.addToCartn(driver, 1);
		driver.findElement(By.xpath("//button[contains(@id,'menu')]")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
		ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@id,'reset')]")));
		driver.findElement(By.xpath("//a[contains(@id,'reset')]")).click();
		try {
		itemsNoS = driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText();
		}
		catch(Exception e) {
			itemsNoS="0";
		}
		String[] resetN= {itemsN.get(0),itemsNoS};
		return resetN;
	}
}
