package AutomationPractice.AP;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class checkoutInfo {

	public static String personalInfo(WebDriver driver, int n, String name, String lastName, String Zip) {
		//initPage.signin(driver, "standard_user", "secret_sauce");
		String text="";
		driver.findElement(By.xpath("//a[contains(@class,'shopping')]")).click();
		driver.findElement(By.id("checkout")).click();
		if (n==0) {
			driver.findElement(By.id("first-name")).sendKeys(name);
			driver.findElement(By.id("continue")).click();
			text = driver.findElement(By.tagName("h3")).getText();
		}
		if (n==1) {
			driver.findElement(By.id("last-name")).sendKeys(lastName);
			driver.findElement(By.id("continue")).click();
			text = driver.findElement(By.tagName("h3")).getText();
		}
		if (n==2) {
			driver.findElement(By.id("postal-code")).sendKeys(Zip);
			driver.findElement(By.id("continue")).click();
			text = driver.findElement(By.tagName("h3")).getText();
		}
		if (n==3) {
			driver.findElement(By.id("first-name")).sendKeys(name);
			driver.findElement(By.id("last-name")).sendKeys(lastName);
			driver.findElement(By.id("continue")).click();
			text = driver.findElement(By.tagName("h3")).getText();
		}
		if (n==4) {
			driver.findElement(By.id("first-name")).sendKeys(name);
			driver.findElement(By.id("postal-code")).sendKeys(Zip);
			driver.findElement(By.id("continue")).click();
			text = driver.findElement(By.tagName("h3")).getText();
		}
		if (n==5) {
			driver.findElement(By.id("last-name")).sendKeys(lastName);
			driver.findElement(By.id("postal-code")).sendKeys(Zip);
			driver.findElement(By.id("continue")).click();
			text = driver.findElement(By.tagName("h3")).getText();
		}
		if (n==6) {
			driver.findElement(By.id("first-name")).sendKeys(name);
			driver.findElement(By.id("last-name")).sendKeys(lastName);
			driver.findElement(By.id("postal-code")).sendKeys(Zip);	
			driver.findElement(By.id("continue")).click();
			text = driver.getCurrentUrl();
		}
		return text;
			
	}
	
}
