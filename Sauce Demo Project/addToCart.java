package AutomationPractice.AP;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class addToCart {
	public static ArrayList<String> addToCartn(WebDriver driver,int nOfItems) {
		initPage.signin(driver, "standard_user", "secret_sauce");
		ArrayList <String> xpath = new ArrayList <String>();
		xpath.add("//button[@id='add-to-cart-sauce-labs-backpack']");
		String itemName = driver.findElement(By.xpath("//div[@class='inventory_item_name']")).getText();
		String itemPrice = driver.findElement(By.xpath("//div[@class='inventory_item_price']")).getText();
		xpath.add("//button[@id='add-to-cart-sauce-labs-bike-light']");
		xpath.add("//button[@id='add-to-cart-sauce-labs-bolt-t-shirt']");
		xpath.add("//button[@id='add-to-cart-sauce-labs-fleece-jacket']");
		xpath.add("//button[@id='add-to-cart-sauce-labs-onesie']");
		xpath.add("//button[contains(@id,'allthethings')]");
		for (int i=0;i<nOfItems;i++) {
			driver.findElement(By.xpath(xpath.get(i))).click();
		}
		String itemsNoS = driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText();
		ArrayList<String> re= new ArrayList<String>();
		re.add(itemsNoS); re.add(itemName); re.add(itemPrice);
		return re;
	}
	public static int removeFromCart(WebDriver driver,int nOfItems) {
		ArrayList <String> xpath = new ArrayList <String>();
		xpath.add("//button[@id='remove-sauce-labs-backpack']");
		xpath.add("//button[@id='remove-sauce-labs-bike-light']");
		xpath.add("//button[@id='remove-sauce-labs-bolt-t-shirt']");
		xpath.add("//button[@id='remove-sauce-labs-fleece-jacket']");
		xpath.add("//button[@id='remove-sauce-labs-onesie']");
		xpath.add("//button[contains(@id,'allthethings')]");
		for (int i=0;i<nOfItems;i++) {
			driver.findElement(By.xpath(xpath.get(i))).click();
		}
		String itemsNoS = driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText();
		int itemsNo = Integer.parseInt(itemsNoS);
		return itemsNo;
	}
	
	public static int selectItemToCart(WebDriver driver, int nOfItems) {
		initPage.signin(driver, "standard_user", "secret_sauce");
		for (int i=0; i<nOfItems;i++) {
			if (i<=nOfItems-2) {
				driver.findElement(By.xpath("(//div[@class='inventory_item_name'])["+(i+1)+"]")).click();
				driver.findElement(By.xpath("//button[contains(@id,'add')]")).click();	
				driver.findElement(By.id("back-to-products")).click();
			}
			else {
				driver.findElement(By.xpath("(//div[@class='inventory_item_name'])["+(i+1)+"]")).click();
				driver.findElement(By.xpath("//button[contains(@id,'add')]")).click();
			}
		}
		String itemsNoS = driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText();
		int itemsNo = Integer.parseInt(itemsNoS);
		return itemsNo;
	}
	
	public static int removeSelectedItem(WebDriver driver) {
		driver.findElement(By.xpath("//button[contains(@id,'remove')]")).click();
		String itemsNoS = driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText();
		int itemsNo = Integer.parseInt(itemsNoS);
		return itemsNo;
	}
	
	public static int removeFromCart(WebDriver driver) {
		driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
		driver.findElement(By.xpath("//button[contains(@id,'remove')]")).click();
		String itemsNoS = driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText();
		int itemsNo = Integer.parseInt(itemsNoS);
		return itemsNo;
		
	}
}
