package AutomationPractice.AP;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import static org.junit.jupiter.api.Assertions.*;

public class checkoutTest {
	@Test
	public void continueShoppingTest() {
		WebDriver driver = initPageTest.init();
		String URL = "https://www.saucedemo.com/inventory.html";
		String URL2 = "https://www.saucedemo.com/checkout-step-one.html";
		String text = checkout.continueShopping(driver);
		assertEquals(text,URL);
		String text2 = checkout.checkoutC(driver);
		assertEquals(text2,URL2);
		driver.quit();
	}

}
