package AutomationPractice.AP;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.*;

public class initPageTest {
	
	static public WebDriver init() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DANY_\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}	
	@Test
	public void testone(){
		WebDriver driver=init();
		initPage.getToPage(driver);	
		driver.quit();
	}	
	@ParameterizedTest
	@CsvFileSource (resources = "/data.csv")
	public void signin(String username, String password) {
		WebDriver driver=init();
		String text = initPage.signin(driver,username,password);
		if (username.equals("locked_out_user")) {			
			assertEquals(text, "Epic sadface: Sorry, this user has been locked out.");
		}
		else if (username.equals("myName")) {
			assertEquals(text, "Epic sadface: Username and password do not match any user in this service");
		}
		else  {
			assertEquals(text,"https://www.saucedemo.com/inventory.html");
		}
		driver.quit();
	}

}
