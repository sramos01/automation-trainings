package AutomationPractice.AP;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import excel.excelReader;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

public class checkTestInfo {
	@Test
	public void personalInfoTest() throws IOException {
		WebDriver driver = initPageTest.init();
		ArrayList <String> texts =  new ArrayList <String>();
		texts.add("Error: Last Name is required");
		texts.add("Error: First Name is required");
		texts.add("Error: First Name is required");
		texts.add("Error: Postal Code is required");
		texts.add("Error: Last Name is required");
		texts.add("Error: First Name is required");
		texts.add("https://www.saucedemo.com/checkout-step-two.html");
		ArrayList<String> a= excelReader.getExcelData();
		String name = a.get(0);
		String lastName = a.get(1);
		String Zip = a.get(2);
		for (int i=0;i<texts.size();i++) {
			String text=checkoutInfo.personalInfo(driver, i, name, lastName, Zip);
			assertEquals(text, texts.get(i));
		}
		driver.quit();
		
		
	}

}
