package AutomationPractice.AP;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import static org.junit.jupiter.api.Assertions.*;

public class filterTest {

	@ParameterizedTest
	@CsvSource (value = {"az,Sauce Labs Backpack","za,Test.allTheThings() T-Shirt (Red)","lohi,Sauce Labs Onesie","hilo,Sauce Labs Fleece Jacket"})
	public void filterTests(String value, String text) {
		WebDriver driver = initPageTest.init();
		String rtext= Filters.filter(driver, value);
		assertEquals(rtext,text);
		driver.quit();
	}
}
