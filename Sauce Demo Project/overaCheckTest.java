package AutomationPractice.AP;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import static org.junit.jupiter.api.Assertions.*;


public class overaCheckTest {
	@Test
	public void productMatchesTest() throws InterruptedException {
		WebDriver driver = initPageTest.init();
		returnoveraCheck result = overaCheck.productMatches(driver);
		assertEquals(result.getProductName1(), result.getProductName2());
		assertEquals(result.getProductPrice1(),result.getProductPrice2());
		float total = result.getProductPrice1()+result.getTax();
		assertEquals(total,result.getTotal());
		Thread.sleep(500);
		String message = "THANK YOU FOR YOUR ORDER";
		String rMessage = overaCheck.finishButton(driver);
		Thread.sleep(500);
		assertEquals(message,rMessage);
		driver.quit();
	}
	
}
