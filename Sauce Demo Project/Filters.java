package AutomationPractice.AP;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class Filters {
	public static String filter(WebDriver driver, String value) {
		initPage.signin(driver, "standard_user", "secret_sauce");
		Select dropdown = new Select(driver.findElement(By.tagName("Select")));
		dropdown.selectByValue(value);
		String text  = driver.findElement(By.xpath("(//div[@class='inventory_item_name'])[1]")).getText();
		return text;
	}

}
