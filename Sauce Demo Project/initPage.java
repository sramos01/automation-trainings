package AutomationPractice.AP;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class initPage {

	public static void getToPage(WebDriver driver) {
		driver.get("https://www.saucedemo.com/");
	}	
	static Logger logger = LogManager.getLogger(initPage.class);
	public static String signin(WebDriver driver, String username, String password) {
		getToPage(driver);
		driver.findElement(By.id("user-name")).sendKeys(username);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.name("login-button")).click();
		String text ="";
		if (username.equals("locked_out_user") || username.equals("myName")) {			
			text = driver.findElement(By.xpath("//h3[@data-test='error']")).getText();
			String fileName = username;
			File ssFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(ssFile, new File(".//screenshot/"+fileName+".png"));
				logger.error("Login using username "+username+" doesnt work");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(username.equals("problem_user")) {
			text = driver.getCurrentUrl();
			String fileName = username;
			File ssFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(ssFile, new File(".//screenshot/"+fileName+".png"));
				logger.debug("Login using username "+username+" works, but has a problem in inventory page");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(username.equals("performance_glitch_user")) {
			text = driver.getCurrentUrl();
			logger.debug("Login using username "+username+" works, but it is slow");			
		}
		else  {
			text = driver.getCurrentUrl();
			logger.trace("Login using username "+username+" works");			
		}
		return text;		
	}
}
