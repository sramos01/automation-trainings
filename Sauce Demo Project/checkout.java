package AutomationPractice.AP;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class checkout {

	public static String continueShopping(WebDriver driver) {
		initPage.signin(driver, "standard_user", "secret_sauce");
		driver.findElement(By.xpath("//a[contains(@class,'shopping')]")).click();
		driver.findElement(By.id("continue-shopping")).click();
		String text = driver.getCurrentUrl();
		return text;		
	}
	
	public static String checkoutC(WebDriver driver) {
		driver.findElement(By.xpath("//a[contains(@class,'shopping')]")).click();
		driver.findElement(By.id("checkout")).click();
		String text = driver.getCurrentUrl();
		return text;
	}
}
