package AutomationPractice.AP;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

public class overaPageTest {
	@Test
	public void logOutTest() {
		WebDriver driver = initPageTest.init();
		String url = overaPage.logOut(driver);
		assertEquals(url, "https://www.saucedemo.com/");
		driver.quit();
	}
	@Test
	public void resetAppTest() {
		WebDriver driver = initPageTest.init();
		String [] result = overaPage.resetApp(driver);
		assertTrue(Integer.parseInt(result[0])>0);
		assertTrue(Integer.parseInt(result[1])==0);
		driver.quit();
	}

}
