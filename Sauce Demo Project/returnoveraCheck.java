package AutomationPractice.AP;

final class returnoveraCheck {
	private final String productName1;
	private final String productName2;
	private final float productPrice1;
	private final float productPrice2;
	private final float tax;
	private final float total;
	
	public returnoveraCheck(String productName1, String productName2, float productPrice1, float productPrice2, float tax,
			float total) {
		super();
		this.productName1 = productName1;
		this.productName2 = productName2;
		this.productPrice1 = productPrice1;
		this.productPrice2 = productPrice2;
		this.tax = tax;
		this.total = total;
	}
	public String getProductName1() {
		return productName1;
	}
	public String getProductName2() {
		return productName2;
	}
	public float getProductPrice1() {
		return productPrice1;
	}
	public float getProductPrice2() {
		return productPrice2;
	}
	public float getTax() {
		return tax;
	}
	public float getTotal() {
		return total;
	}
	
	
	
	
	

}
