package AutomationPractice.AP;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

public class cartTest {
	@Test
	public void CartTest() {
		WebDriver driver = initPageTest.init();
		int nOfItems = 3;
		ArrayList<String> itemsNo = addToCart.addToCartn(driver, nOfItems);
		assertTrue(Integer.parseInt(itemsNo.get(0))==nOfItems);
		int nOfItems2 = 2;
		int itemsNoR = addToCart.removeFromCart(driver, nOfItems2);
		assertTrue(nOfItems-nOfItems2 == itemsNoR );
		driver.quit();
	}
	
	@Test
	public void selectItemsToCart() {
		WebDriver driver = initPageTest.init();
		int nOfItems = 3;
		int itemsNo = addToCart.selectItemToCart(driver, nOfItems);
		assertTrue(nOfItems == itemsNo);
		int itemsNo2 = addToCart.removeSelectedItem(driver);
		assertTrue(itemsNo2 == nOfItems-1);
		int itemsNo3 = addToCart.removeFromCart(driver);
		assertTrue(itemsNo3 == nOfItems - 2);
		driver.quit();
	}
	
}
