package Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Form {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DANY_\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		driver.findElement(By.id("first-name")).sendKeys("Daniela");
		driver.findElement(By.id("last-name")).sendKeys("Ramos");
		driver.findElement(By.id("job-title")).sendKeys("Automation Trainee");
		driver.findElement(By.cssSelector("input[value='radio-button-2']")).click();
		driver.findElement(By.cssSelector("input[value*='box-2']")).click();
		Select dropdown = new Select(driver.findElement(By.id("select-menu")));
		dropdown.selectByValue("1");
		driver.findElement(By.xpath("//input[@id='datepicker']"))
		.sendKeys("01/01/2021");
		driver.findElement(By.xpath("//a[@role='button']")).click();
	}

}
