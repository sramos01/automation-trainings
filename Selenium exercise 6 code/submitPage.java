package pages;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class submitPage {

	public static String thanksMessage(WebDriver driver)  {
		formpage.getToPage(driver);
		driver.findElement(By.xpath("//a[@role='button']")).click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'form')]")));
		driver.findElement(By.xpath("//div[contains(text(),'form')]")).getText();		
		String text = driver.findElement(By.xpath("//h1[contains(text(),'Thank')]")).getText();
		String fileName = "Submit";
		File ssFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(ssFile, new File(".//screenshot/"+fileName+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return text;
	}

}
