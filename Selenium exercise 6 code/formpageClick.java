package pages;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class formpageClick {

	public static Boolean highestEducation(WebDriver driver)  {
		formpage.getToPage(driver);
		driver.findElement(By.id("radio-button-2")).click();
		Boolean enabled =  driver.findElement(By.id("radio-button-2")).isSelected();
		String fileName = "Checkbox";
		File ssFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(ssFile, new File(".//screenshot/"+fileName+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return enabled;
	}
	
	public static Boolean sex(WebDriver driver) {
		formpage.getToPage(driver);
		driver.findElement(By.id("checkbox-2")).click();
		Boolean enabled =  driver.findElement(By.id("checkbox-2")).isSelected();
		return enabled;
	}
	
	public static String dropdown(WebDriver driver, String value)  {
		formpage.getToPage(driver);
		Select dropdown = new Select(driver.findElement(By.id("select-menu")));
		dropdown.selectByValue(value);
		String value1 = driver.findElement(By.id("select-menu")).getAttribute("value");
		String fileName = "Select";
		File ssFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(ssFile, new File(".//screenshot/"+fileName+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value1;
	}
}
