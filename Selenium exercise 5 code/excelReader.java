package excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class excelReader {
 
	public static ArrayList<String> getExcelData() throws IOException {
		 FileInputStream fis =  new FileInputStream("C:\\Users\\DANY_\\Selenium-training\\setup-junit-eclipse\\src\\main\\java\\excel\\data.xlsx");
		 XSSFWorkbook workbook = new XSSFWorkbook(fis);
		 XSSFSheet sheet = workbook.getSheet("formData");
		 Iterator <Row> rows = sheet.iterator();
		 Row firstrow = rows.next();
		 Iterator <Cell> ce = firstrow.cellIterator();
		 Cell value = ce.next();
		 String name = value.getStringCellValue();		
		 Cell value1 = ce.next();
		 String lastName = value1.getStringCellValue();		
		 Cell value2 = ce.next();
		 String jobTitle = value2.getStringCellValue();		 
		 ArrayList<String> data = new ArrayList<String>();
		 data.add(name);
		 data.add(lastName);
		 data.add(jobTitle);
		 return data;
		 }
}
